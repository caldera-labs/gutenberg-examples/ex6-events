# Example Of A Fairly Complex Gutenberg Block.
<em>This plugin is a work in progress. Very early. Not complete.</em>

## Plugin Features and Educational Concepts
* webpack
* JSX, compiled using WordPress render function
* WordPress babel presets.
* Importing components from Gutenberg using the `@wordpress` shortcut.
* Event post type with blocks for title, description, and date.
* RSVP post type for event rsvps. Block for RSVP list by event and RSVPing (front-end or back-end)
* Non-Gutenberg wp-admin screen using the same components as the block interface.
* Sharing data between blocks using `wp.data`.
* Using `wp.hooks` to clone core blocks.
* All components use `wp.data` for state management, in Gutenberg and not in Gutenberg.
* Predefined, multi-column ?multi-column blocks/nested blocks/? combinging smaller blocks
* Saving semantic HTML to represent meta data and storing it as meta data.
    * No-JavaScript fallback by default, progressively enhance with JavaScript and CSS when possible.
    * Can use block attributes with WP_Query, since they are in the database as meta fields. Or access attributes using get_post_meta().
* Using React DOM to mount some of the components from wp-admin and post editor in the front-end.

## Development
* `git clone https://gitlab.com/caldera-labs/gutenberg-examples/ex6-events.git`
* `cd ex6-events`
* `npm install`


## Prior Art
This plugin has a lot of copypaste form [https://github.com/youknowriad/gcf](GCF) by [Riad Benguella](https://riad.blog/), which is a very cool plugin to read through as an example of Gutenberg block plugin.