<?php

/**
 * Enqueue assets needed for the admin editor.
 *
 * Use the "enqueue_block_assets" action to enqueue
 * assets on the front-end of your website.
 */
function learn_gutenberg_ex6_events_enqueue_block_editor_assets() {
    $dir = dirname( __FILE__ );
    $block_js = '/build/index.js';
    $editor_css = '/build/style.css';
    $handle = 'learn-gutenberg/ex6-events';

    global $wp_version;
    wp_enqueue_script( $handle, plugins_url( $block_js, __FILE__ ), array(
        'wp-blocks',
    ), md5(md5_file( "$dir/$block_js" ) ) . $wp_version );

    wp_enqueue_style( $handle, plugins_url( $editor_css, __FILE__ ), array(
        'wp-blocks',
        //CSS for components, we're re-using them.
        'wp-components'
    ), filemtime( "$dir/$editor_css" ) );
}
add_action( 'enqueue_block_editor_assets', 'learn_gutenberg_ex6_events_enqueue_block_editor_assets' );




// Register Custom Post Types
function learn_gutenberg_ex6_events_post_type() {

    //Register event post type
    $event_post_type_name = 'ex6-events-event';
    $args = array(
        'label'                 => __( 'Event', 'learn-gutenberg' ),
        'description'           => __( 'Events', 'learn-gutenberg' ),
        'supports'              => array( 'title', 'editor' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        //Show in rest api at /wp/v2/events
        'show_in_rest' => true,
        'rest_base' => 'events',
        'template_lock' => 'insert'
    );
    register_post_type( $event_post_type_name, $args );

    //Register rsvp as a hidden post type
    $rsvp_post_type_name = 'ex6-events-rsvps';
    $args = array(
        'label'                 => __( 'RSVP', 'learn-gutenberg' ),
        'description'           => __( 'RSVPs', 'learn-gutenberg' ),
        'supports'              => array( 'title', 'custom-fields' ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => false,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        //Show in rest api at /wp/v2/rsvps
        'show_in_rest' => true,
        'rest_base' => 'rsvps'
    );
    register_post_type( $rsvp_post_type_name, $args );


    //Define meta keys per post type
    $meta_keys_by_post_type = array(
        $rsvp_post_type_name => array(
            'learn_gutenberg_ex6_rsvp_name',
            'learn_gutenberg_ex6_rsvp_email',
            'learn_gutenberg_ex6_rsvp_type',
        ),
        $event_post_type_name => array(
            'learn_gutenberg_ex6_event_description'
        )
    );

    //Register our meta keys
    foreach ( $meta_keys_by_post_type as $post_type => $keys ){
        foreach ( $keys as $key ){
            register_meta( $post_type, $key, array(
                'show_in_rest' => true,
                'single' => true
            ) );
        }

    }

    //When RSVP is submitted via REST API, update its status to publish right away
    add_action( "rest_insert_$rsvp_post_type_name", function( $post, $request ) use( $meta_keys_by_post_type, $rsvp_post_type_name ){
        $post->post_status = 'publish';
        wp_update_post( $post );
        if ( $request['meta'] ) {
            $metas  = get_post_meta($post->ID );
            foreach ($meta_keys_by_post_type[$rsvp_post_type_name] as $key) {
                if (!$metas[$key] && $request['meta'][$key]) {
                    update_post_meta($post->ID, $key, $request['meta'][$key]);
                }
            }
        }
    }, 10, 2 );


}
add_action( 'init', 'learn_gutenberg_ex6_events_post_type', 0 );

