/**
 * This file is everything we need to represent Event Description, as a block.
 *
 * The block is not registered here, index.js has the responsibility of registering blocks
 */
//@TODO import from webpack
const __ = wp.i18n.__;

//import CSS
//import './editor.scss';

//Import class names
import classNames from '../classNames';

//Import components for UI
import {EventDescription} from "../components/EventDescription/index";

//Export block name for consistency
export const EventDescriptionBlockName = 'learn-gutenberg/ex6-events-description';

//Export block definition, decoupled from block Registration
export const EventDescriptionBlock = {
    title: __( 'Event Description', 'learn-gutenberg'),
    category: 'common',
    attributes: {
        eventDescription: {
            //Should be saved as meta, but can not
            //https://github.com/WordPress/gutenberg/issues/4989
            type: 'text',
            selector: `.${classNames.description}`,
            default: __( 'Apex Event', 'learn-gutenberg' )
        }
    },
    //Edit callback - Mounts and manages state of decoupled components in Gutenberg context.
    edit({ attributes, setAttributes, isSelected, id }) {
        /**
         * Change handler to update state in Gutenberg
         */
        const ChangeHandler = (newValue) => {
            setAttributes({eventDescription:newValue});
        };

        //Displays edit component when selected, display preview when not selected
        return (
            <div className={classNames.description}>
                {isSelected &&
                    <EventDescription.edit
                        onChange={ChangeHandler}
                        blockId={`${classNames.description}-${id}`}
                        value={attributes.eventDescription}
                        attribute={'eventDefault'}
                    />

                }

                {! isSelected &&
                    EventDescription.display(attributes.eventDescription)
                }

            </div>
        );
    },
    //Save callback, uses same component as front-end
    save({ attributes } ) {
        return EventDescription.display(attributes.eventDescription);
    }
};
