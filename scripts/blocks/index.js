//Import block registration API form WordPress
import { registerBlockType } from "@wordpress/blocks";

//Collect blocks during registration
const AllBlocks = {};

//Register blocks
//Register Description Block
import {EventDescriptionBlockName,EventDescriptionBlock} from "./EventDescription";
registerBlockType(EventDescriptionBlockName,EventDescriptionBlock);
AllBlocks[EventDescriptionBlockName] = EventDescriptionBlock;

//Dispatch action after blocks are registered
wp.hooks.doAction( 'learnGutenberg.eventsBlocks.registered', AllBlocks );

