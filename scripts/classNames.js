/**
 * One container for all class names so they stay consistent in edit/save callbacks and the other places they are used
 */
export default  {
    description: 'learn-gutenberg-event-description',
    link: 'learn-gutenberg-event-link',
    date: 'learn-gutenberg-event-date',
    rsvp: 'learn-gutenberg-event-rsvp',
    rsvps: 'learn-gutenberg-event-list-rsvp',
    createRsvp: 'learn-gutenberg-event-create-rsvp',
    rsvpName: 'learn-gutenberg-event-rsvp-name',
    rsvpEmail: 'learn-gutenberg-event-rsvp-email',
    rsvpType: 'learn-gutenberg-event-rsvp-type',
}