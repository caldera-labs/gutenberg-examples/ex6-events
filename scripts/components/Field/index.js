import { withInstanceId } from "@wordpress/components";

import "./style.scss";

/**
 * Component for placing UI controls in
 *
 * Based on: https://github.com/youknowriad/gcf/tree/master/scripts/blocks/components/field
 *
 * @param instanceId
 * @param label
 * @param children
 * @param labelBefore
 * @returns {XML}
 * @constructor
 */
function Field({ instanceId, label, children,labelBefore = true }) {
  const id = `learn-gutenberg-events-${instanceId}`;
  return (
    <div className="learn-gutenberg-events-field">
        {labelBefore &&
            <label htmlFor={id}>{label}</label>
        }
          {children(id)}
        {! labelBefore &&
            <label htmlFor={id}>{label}</label>
        }
    </div>
  );
}

//Return wrapped in the WordPress HoC withInstanceId
export default withInstanceId(Field);
