//import CSS
import "./style.scss";
//Import class names reference
import classNames from '../../../classNames';

/**
 * Create preview/display view for event description
 */
export const DisplayEventDescription = (fieldValue) => {
    return (
        <div
            className={classNames.description}
        >
            {fieldValue}
        </div>
    );
};

