//Combine components to one export
import {DisplayEventDescription} from "./Display/index";
import {EditEventDescription} from "./Edit/index";

//Export as edit/display preview
export const EventDescription = {
    display: DisplayEventDescription,
    edit: EditEventDescription
};