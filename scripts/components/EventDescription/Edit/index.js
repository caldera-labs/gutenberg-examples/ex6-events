//import CSS
import "./style.scss";
//Extend WordPress component instead of React
import {Component} from "@wordpress/element";
//@TODO import from webpack
const __ = wp.i18n.__;
//Import TextControl from core
import {TextControl} from '@wordpress/components';
import Field from '../../Field';
/**
 * Event description editor class
 */
export class EditEventDescription extends Component {
    //Constructor
    constructor(props) {
        //Pass props to super class
        super(props);
        //Bind this to onChange function so we can access props/state or update state.
        this.onChange = this.onChange.bind(this);
    }

    //Change handler
    onChange(newValue){
        //Pass new value up to Gutenberg or other state management
        this.props.onChange(newValue);
    }

    //Edit interface
    render(){
        return (
            <div>
                <Field
                    label={__( 'Description for event', 'learn-gutenberg' )}
                    instanceId={this.props.blockId}
                    labelBefore={false}
                >
                    {id => (
                        <TextControl
                            id={id}
                            type="text"
                            value={this.props.value}
                            onChange={this.onChange}
                            placeholder={this.props.placeholder}
                        />
                    )}
                </Field>

            </div>
        );
    }

}