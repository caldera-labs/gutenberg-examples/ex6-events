//Import WordPress render functions (instead of using react-dom and react.createElement directly)
import { render, createElement } from "@wordpress/element";

//Display component only (no edit)
import {DisplayEventDescription} from "../components/EventDescription/Display/index";

//Class names for consistency
import classNames from '../classNames';

//Find all static blocks that are description blocks and replace with React app.
document.addEventListener('DOMContentLoaded', function(event) {
    //Find elements by class
    const descriptionBlocks = document.getElementsByClassName(`${classNames.description}`);

    //Check if we found any
    if ( descriptionBlocks.length) {
        //Loop through each
        descriptionBlocks.forEach( (blockDOMElement) => {
            //Get content
            //This could be REST API call or some other source
            let content = blockDOMElement.innerHTML;
            //Mount app over same DOM element
            render(
                //Create HTML out of
                createElement(
                    'div',
                    {
                        className: `${classNames.description}-front-outer`
                    },
                    DisplayEventDescription(content),
                ),
                blockDOMElement
            );
        });
    }
});

